package com.example.glidefirsttest;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class MainActivity extends Activity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.view_image);

        loadImage();
    }

    private void loadImage() {

        Glide.with(this).
                load(R.drawable.studio).
                into(imageView);
    }
}
