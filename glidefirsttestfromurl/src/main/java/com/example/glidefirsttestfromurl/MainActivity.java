package com.example.glidefirsttestfromurl;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.view_image);
        accesToUrl();

    }
    private void accesToUrl() {

        new AsyncTask<String, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(String... params) {
                return loadPicture(params[0]);
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                if (result == null) {
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.placeholder));
                } else {
                    imageView.setImageBitmap(result);
                }
            }
        }.execute("https://www.google.es/images/srpr/logo11w.png");
    }


    private Bitmap loadPicture(String url) {

        Bitmap theBitmap = null;
        try {
            theBitmap = Glide.
                    with(this).
                    load(url).
                    asBitmap().
                    into(100, 100). // Width and height
                    get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return theBitmap;
    }
}

