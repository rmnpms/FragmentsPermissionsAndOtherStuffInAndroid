package com.example.firstexercicisewithrealm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import io.realm.Realm;

public class IntroExampleActivity extends AppCompatActivity {

    public static final String TAG = IntroExampleActivity.class.getName();
    private LinearLayout linearLayout;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_example);

        linearLayout = (LinearLayout) findViewById(R.id.container);

        // Create the Realm instance
        realm = Realm.getDefaultInstance();



    }
}
