package com.example.twopermissionsrequestmarshmallow;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    final private String[] permissions = {Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        askForPermission(permissions, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
    }

    private void askForPermission(String permission[], Integer requestCode) {

        for (int i = 0; i < permission.length; i++) {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    permission[i]) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale
                        (MainActivity.this, permission[i])) {

                    //This is called if user has denied the permission before
                    //In this case I am just asking the permission again
                    ActivityCompat.requestPermissions(MainActivity.this, permission, requestCode);

                } else {

                    ActivityCompat.requestPermissions(MainActivity.this, permission, requestCode);
                }
            } else {
                Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
